with open("../input.txt", "r") as f:
    total = 0
    for line in f.readlines():
        line = "".join(filter(str.isnumeric, line))
        cal = line[0] + line[len(line)-1]
        total += int(cal)
        print(cal)
    print(total)