with open("../input.txt", "r") as f:
    total = 0
    for line in f.readlines():
        num_out = ""
        for i, char in enumerate(line):
            if char == 'o':
                if line[i:i+3] == 'one':
                    num_out += "1"
            elif char == 't':
                if line[i:i+3] == 'two':
                    num_out += "2"
                if line[i:i+5] == 'three':
                    num_out += "3"
            elif char == 'f':
                if line[i:i+4] == 'four':
                    num_out += "4"
                elif line[i:i+4] == 'five':
                    num_out += "5"
            elif char == 's':
                if line[i:i+3] == 'six':
                    num_out += "6"
                elif line[i:i+5] == 'seven':
                    num_out += "7"
            elif char == 'e':
                if line[i:i+5] == 'eight':
                    num_out += "8"
            elif char == 'n':
                if line[i:i+4] == 'nine':
                    num_out += "9"
            else:
                try:
                    num_out += str(int(char))
                except ValueError:
                    pass
        cal = num_out[0] + num_out[len(num_out)-1]
        print(line, num_out, cal)
        total += int(cal)
    print(total)