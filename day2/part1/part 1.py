from operator import mul
max_val = {
    "blue" : 14,
    "red" : 12,
    "green" : 13
    }
def game_sim(line: str):
    line = line.split(":")
    game = int(line[0].split(" ")[1])
    results = []
    for set in line[1].split(";"):
        sub = {}
        for pull in set.split(","):
            pull = pull.lstrip().strip()
            counts, colour= pull.split(" ")
            sub[colour] = int(counts)
        results.append(sub)
    return game, results

def playable(subs:list):
    for sub in subs:
        for colour, count in sub.items():
            if max_val[colour] < count:
                return False
    return True


if __name__ == "__main__":
    game_sum = 0
    with open("../input.txt", "r") as f:
        for line in f.readlines():
            game_num, subs = game_sim(line)
            game_sum += game_num if playable(subs) else 0
        print(game_sum)