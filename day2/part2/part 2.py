from operator import mul
def game_sim(line: str):
    line = line.split(":")
    game = int(line[0].split(" ")[1])
    results = []
    for set in line[1].split(";"):
        sub = {}
        for pull in set.split(","):
            pull = pull.lstrip().strip()
            counts, colour= pull.split(" ")
            sub[colour] = int(counts)
        results.append(sub)
    return game, results

def powers(subs:list):
    min = {
        "red": 0,
        "green": 0,
        "blue": 0
    }
    for sub in subs:
        for colour, count in sub.items():
            min[colour] = max(min[colour], count)
    power = mul(mul(min["red"], min["green"]), min["blue"])
    return power

if __name__ == "__main__":
    game_power = 0
    with open("../input.txt", "r") as f:
        for line in f.readlines():
            game_num, subs = game_sim(line)
            game_power += powers(subs)
        print(game_power)